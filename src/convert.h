/*******************************************************************************
    This file is part of bhacon.

    bhacon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    bhacon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with bhacon.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef CONVERT_H
#define CONVERT_H

#include <gtk/gtk.h>
#include "main_window.h"

/*****************************************************************************/
/*! \brief Convert from one type to all other.                               */
/*****************************************************************************/
void convert_to_all(
    gchar* text_view_str,       /*!< Text view string */
    text_buffer_t text_type     /*!< Text view type to convert from */
    );

/*****************************************************************************/
/*! \brief Get converted binary string.                                      */
/*! \return pointer to binary string                                         */
/*****************************************************************************/
gchar* bin_str_get(
    void
    );

/*****************************************************************************/
/*! \brief Get converted hexadecimal string.                                 */
/*! \return pointer to hexadecimal string                                    */
/*****************************************************************************/
gchar* hex_str_get(
    void
    );

/*****************************************************************************/
/*! \brief Get converted ASCII string.                                       */
/*! \return pointer to ASCII string                                          */
/*****************************************************************************/
gchar* ascii_str_get(
    void
    );

/*****************************************************************************/
/*! \brief Free the memory for converted strings.                            */
/*****************************************************************************/
void free_strs(
    void
    );

#endif  /* #ifndef CONVERT_H */
