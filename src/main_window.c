/*******************************************************************************
    This file is part of bhacon.

    bhacon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    bhacon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with bhacon.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <gtk/gtk.h>
#include "main_window.h"
#include "convert.h"

/* LOCAL FUNCTION PROTOTYPES *************************************************/
static gchar* buffer_get_text(GtkTextBuffer *buffer);
static void on_button_clear_clicked(void);
static void on_button_convert_clicked(void);
static void on_buffer_bin_changed(void);
static void on_buffer_hex_changed(void);
static void on_buffer_ascii_changed(void);

/* LOCAL VARIABLES ***********************************************************/
static text_buffer_t text_buffer_new = TEXT_BUFFER_NONE;

static GtkTextBuffer *buffer_bin;
static GtkTextBuffer *buffer_hex;
static GtkTextBuffer *buffer_ascii;
static GtkWidget *text_view_bin;
static GtkWidget *text_view_hex;
static GtkWidget *text_view_ascii;

/* GLOBAL FUNCTIONS **********************************************************/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void main_window_init(gchar *ui)
{
    GtkBuilder *builder;
    GtkWidget *window;
    GtkWidget *button_clear;
    GtkWidget *button_convert;

    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, ui, NULL);

    /* window_start configs */
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_start"));
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    /* button_clear configs */
    button_clear = GTK_WIDGET(gtk_builder_get_object(builder, "button_clear"));
    g_signal_connect(button_clear, "clicked", G_CALLBACK(on_button_clear_clicked), NULL);

    /* button_convert configs */
    button_convert = GTK_WIDGET(gtk_builder_get_object(builder, "button_convert"));
    g_signal_connect(button_convert, "clicked", G_CALLBACK(on_button_convert_clicked), NULL);

    /* text view configs */
    text_view_bin = GTK_WIDGET(gtk_builder_get_object(builder, "text_view_bin"));
    text_view_hex = GTK_WIDGET(gtk_builder_get_object(builder, "text_view_hex"));
    text_view_ascii = GTK_WIDGET(gtk_builder_get_object(builder, "text_view_ascii"));
    buffer_bin = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_bin));
    buffer_hex = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_hex));
    buffer_ascii = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view_ascii));
    g_signal_connect(buffer_bin, "changed", G_CALLBACK(on_buffer_bin_changed), NULL);
    g_signal_connect(buffer_hex, "changed", G_CALLBACK(on_buffer_hex_changed), NULL);
    g_signal_connect(buffer_ascii, "changed", G_CALLBACK(on_buffer_ascii_changed), NULL);

    g_object_unref(G_OBJECT(builder));
    gtk_widget_show(window);
}

/* LOCAL FUNCTIONS ***********************************************************/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static gchar* buffer_get_text(GtkTextBuffer *buffer)
{
    GtkTextIter start;
    GtkTextIter end;

    gtk_text_buffer_get_start_iter(buffer, &start);
    gtk_text_buffer_get_end_iter(buffer, &end);

    return gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void on_button_clear_clicked(void)
{
    gtk_text_buffer_set_text(buffer_bin, "", 0);
    gtk_text_buffer_set_text(buffer_hex, "", 0);
    gtk_text_buffer_set_text(buffer_ascii, "", 0);
    text_buffer_new = TEXT_BUFFER_NONE;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void on_button_convert_clicked(void)
{
    switch (text_buffer_new)
    {
    case TEXT_BUFFER_BIN:
        convert_to_all(buffer_get_text(buffer_bin), TEXT_BUFFER_BIN);
        gtk_text_buffer_set_text(buffer_hex, hex_str_get(), -1);
        gtk_text_buffer_set_text(buffer_ascii, ascii_str_get(), -1);
        break;
    case TEXT_BUFFER_HEX:
        convert_to_all(buffer_get_text(buffer_hex), TEXT_BUFFER_HEX);
        gtk_text_buffer_set_text(buffer_bin, bin_str_get(), -1);
        gtk_text_buffer_set_text(buffer_ascii, ascii_str_get(), -1);
        break;
    case TEXT_BUFFER_ASCII:
        convert_to_all(buffer_get_text(buffer_ascii), TEXT_BUFFER_ASCII);
        gtk_text_buffer_set_text(buffer_bin, bin_str_get(), -1);
        gtk_text_buffer_set_text(buffer_hex, hex_str_get(), -1);
        break;
    case TEXT_BUFFER_NONE:
    default:
        return;
    }

    free_strs();
    text_buffer_new = TEXT_BUFFER_NONE;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void on_buffer_bin_changed(void)
{
    if (buffer_get_text(buffer_bin))
    {
        text_buffer_new = TEXT_BUFFER_BIN;
    }
    else
    {
        text_buffer_new = TEXT_BUFFER_NONE;
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void on_buffer_hex_changed(void)
{
    if (buffer_get_text(buffer_hex))
    {
        text_buffer_new = TEXT_BUFFER_HEX;
    }
    else
    {
        text_buffer_new = TEXT_BUFFER_NONE;
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void on_buffer_ascii_changed(void)
{
    if (buffer_get_text(buffer_ascii))
    {
        text_buffer_new = TEXT_BUFFER_ASCII;
    }
    else
    {
        text_buffer_new = TEXT_BUFFER_NONE;
    }
}
