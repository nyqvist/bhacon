/*******************************************************************************
    This file is part of bhacon.

    bhacon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    bhacon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with bhacon.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include "main_window.h"
#include "convert.h"

/* LOCAL FUNCTION PROTOTYPES *************************************************/
static void convert_bin_to_int(gchar* text_view_str);
static void convert_hex_to_int(gchar* text_view_str);
static void convert_ascii_to_int(gchar* text_view_str);
static void convert_to_bin(void);
static void convert_to_hex(void);
static void convert_to_ascii(void);
static void free_str(GString* string);
static gboolean validate_str(gchar* str, gint max_len);

/* CONSTANTS / MACROS ********************************************************/
#define INT8_HIGH(v) (((v) & 0xF0) >> 4)
#define INT8_LOW(v) ((v) & 0x0F)

#define CHAR_NOT_FOUND '.'
#define CHAR_PRINT_START ' '

/* LOCAL VARIABLES ***********************************************************/
static GString* p_bin_str;
static GString* p_hex_str;
static GString* p_ascii_str;
static GArray* p_int_arr;

static gsize text_len;

static const gchar* bin_val[] =
{
    "0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
    "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"
};

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void convert_to_all(gchar* text_view_str, text_buffer_t text_type)
{
    text_len = strlen(text_view_str);

    if (!validate_str(text_view_str, -1))
    {
        return;
    }

    switch (text_type)
    {
    case TEXT_BUFFER_BIN:
        convert_bin_to_int(text_view_str);
        break;
    case TEXT_BUFFER_HEX:
        convert_hex_to_int(text_view_str);
        break;
    case TEXT_BUFFER_ASCII:
        convert_ascii_to_int(text_view_str);
        break;
    default:
    case TEXT_BUFFER_NONE:
        return;
    }

    p_bin_str = g_string_new(NULL);
    p_hex_str = g_string_new(NULL);
    p_ascii_str = g_string_new(NULL);

    if (text_type != TEXT_BUFFER_BIN)
    {
        convert_to_bin();
    }
    if (text_type != TEXT_BUFFER_HEX)
    {
        convert_to_hex();
    }
    if (text_type != TEXT_BUFFER_ASCII)
    {
        convert_to_ascii();
    }

    g_array_free(p_int_arr, FALSE);
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
gchar* hex_str_get(void)
{
    return p_hex_str->str;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
gchar* bin_str_get(void)
{
    return p_bin_str->str;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
gchar* ascii_str_get(void)
{
    return p_ascii_str->str;
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
void free_strs(void)
{
    free_str(p_bin_str);
    free_str(p_hex_str);
    free_str(p_ascii_str);
}

/* LOCAL FUNCTIONS ***********************************************************/
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void free_str(GString* string)
{
    if (string != NULL)
    {
        g_string_free(string, TRUE);
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static gboolean validate_str(gchar* str, gint max_len)
{
    const gchar* p_end = NULL;

    /* Validate the text string */
    if (!g_utf8_validate(str, max_len, &p_end))
    {
        /* TODO: Write to status line that there was an error */
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

/*---------------------------------------------------------------------------*/
/* TODO: Check if string is binary format */
/*---------------------------------------------------------------------------*/
static void convert_bin_to_int(gchar* text_view_str)
{
    gsize i = 0;
    gchar* p_end = NULL;
    gchar tmp_split_str[8] = {0};
    guint int_val = 0;
    p_int_arr = g_array_new(FALSE, FALSE, sizeof(guint));

    while (i < text_len)
    {
        g_utf8_strncpy(tmp_split_str, text_view_str+i, 8);
        int_val = (guint) strtoul(tmp_split_str, &p_end, 2);
        g_array_append_val(p_int_arr, int_val);
        i += 8;
    }
}

/*---------------------------------------------------------------------------*/
/* TODO: Check if string is hexadecimal format */
/*---------------------------------------------------------------------------*/
static void convert_hex_to_int(gchar* text_view_str)
{
    gsize i = 0;
    gchar* p_end = NULL;
    gchar tmp_split_str[2] = {0};
    guint int_val = 0;
    p_int_arr = g_array_new(FALSE, FALSE, sizeof(guint));

    while (i < text_len)
    {
        g_utf8_strncpy(tmp_split_str, text_view_str+i, 2);
        int_val = (guint) strtoul(tmp_split_str, &p_end, 16);
        g_array_append_val(p_int_arr, int_val);
        i += 2;
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void convert_ascii_to_int(gchar* text_view_str)
{
    gsize i = 0;
    guint int_val = 0;
    p_int_arr = g_array_new(FALSE, FALSE, sizeof(guint));

    for (i = 0; i < text_len; i++)
    {
        int_val = (guint) text_view_str[i];
        g_array_append_val(p_int_arr, int_val);
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void convert_to_bin(void)
{
    gsize i = 0;
    guint val = 0;

    for (i = 0; i < p_int_arr->len; i++)
    {
        val = g_array_index(p_int_arr, guint, i);
        g_string_append(p_bin_str, bin_val[INT8_HIGH(val)]);
        g_string_append(p_bin_str, bin_val[INT8_LOW(val)]);
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void convert_to_hex(void)
{
    gsize i = 0;

    for (i = 0; i < p_int_arr->len; i++)
    {
        g_string_append_printf(p_hex_str, "%X",
                               g_array_index(p_int_arr, guint, i));
    }
}

/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
static void convert_to_ascii(void)
{
    gsize i = 0;
    gchar next_char = CHAR_NOT_FOUND;

    for (i = 0; i < p_int_arr->len; i++)
    {
        next_char = (gchar) g_array_index(p_int_arr, guint, i);

        /* Make sure the character can be printed out */
        if (next_char <= CHAR_PRINT_START)
        {
            next_char = CHAR_NOT_FOUND;
        }

        if (validate_str(&next_char, 1))
        {
            g_string_append_printf(p_ascii_str, "%c", next_char);
        }
        else
        {
            g_string_append_printf(p_ascii_str, "%c", CHAR_NOT_FOUND);
        }
    }
}
