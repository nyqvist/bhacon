/*******************************************************************************
    This file is part of bhacon.

    bhacon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    bhacon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with bhacon.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <gtk/gtk.h>

/*****************************************************************************/
/* \breif Holds different text view types.                                   */
/*                                                                           */
/* Used to see which text area is the latest edited one.                     */
/*****************************************************************************/
typedef enum
{
    TEXT_BUFFER_NONE,           /*!< Default, all text views are inactive */
    TEXT_BUFFER_BIN,            /*!< Binary text view is the last updated view*/
    TEXT_BUFFER_HEX,            /*!< Hex text view is the last updated view */
    TEXT_BUFFER_ASCII           /*!< ASCII text view is the last updated view*/
} text_buffer_t;

/*****************************************************************************/
/* \breif Initialize and setup main window and its components                */
/*****************************************************************************/
void main_window_init(
    gchar *ui                   /*!< window UI file name */
    );

#endif  /* #ifndef MAIN_WINDOW_H */
